package com.here.acs.slogs.datatypes;


public class LocationInfo {
	private int lineNumber;
	private String fileName;
	private String className;
	private String methodName;
	
	public LocationInfo() {}
	
	public LocationInfo(String fileName, String className, String methodName, int lineNumber) {
    	this.lineNumber = lineNumber;
    	this.fileName = fileName;
    	this.className = className;
    	this.methodName = methodName;
	}
	
	public int getLineNumber() {
		return lineNumber;
	}
	
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	public String getMethodName() {
		return methodName;
	}
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public static org.apache.log4j.spi.LocationInfo toLog4jLocationInfo(LocationInfo li) {
		if (li == null)
			return null;
		
		return new org.apache.log4j.spi.LocationInfo(
				li.getFileName(), li.getClassName(), li.getMethodName(), String.valueOf(li.getLineNumber()));
	}
	
	public static LocationInfo fromLog4jLocationInfo(org.apache.log4j.spi.LocationInfo li) {
		if (li == null)
			return null;
		
		int lineNo = 0;
		try {
			lineNo = Integer.parseInt(li.getLineNumber());
		} catch (NumberFormatException leaveAsZero) {}
		return new LocationInfo(li.getFileName(), li.getClassName(), li.getMethodName(), lineNo);
	}
	
	public static StackTraceElement toStackTraceElement(LocationInfo li) {
		return new StackTraceElement(li.getClassName(), li.getMethodName(), li.getFileName(), li.getLineNumber());
	}
	
	public static LocationInfo fromStackTraceElement(StackTraceElement ste) {
		return new LocationInfo(ste.getFileName(), ste.getClassName(), ste.getMethodName(), ste.getLineNumber());
	}
}
