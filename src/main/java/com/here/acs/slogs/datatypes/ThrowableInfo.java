package com.here.acs.slogs.datatypes;

import java.util.Arrays;

import org.apache.log4j.spi.ThrowableInformation;

public class ThrowableInfo {
	private String clazz;
	private String message;
	private LocationInfo[] stackTrace;
	private ThrowableInfo cause;
	
	public ThrowableInfo() {}
	
	public ThrowableInfo(String clazz, String message, LocationInfo[] stackTrace, ThrowableInfo cause) {
		this.clazz = clazz;
		this.message = message;
		this.stackTrace = stackTrace;
		this.cause = cause;
	}
	
	public String getClazz() {
		return clazz;
	}
	
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public LocationInfo[] getStackTrace() {
		return stackTrace;
	}
	
	public void setStackTrace(LocationInfo[] stackTrace) {
		this.stackTrace = stackTrace;
	}
	
	public ThrowableInfo getCause() {
		return cause;
	}
	
	public void setCause(ThrowableInfo cause) {
		this.cause = cause;
	}
	
	public static ThrowableInfo fromThrowable(Throwable t) {
		if (t == null)
			return null;
		
		ThrowableInfo info = new ThrowableInfo();
		info.setClazz(t.getClass().getName());
		info.setMessage(t.getMessage());
		info.setCause((t.getCause() == t) ? null : fromThrowable(t.getCause()));
		if (t.getStackTrace() != null)
			info.setStackTrace(Arrays.stream(t.getStackTrace()).map(LocationInfo::fromStackTraceElement).toArray(size -> new LocationInfo[size]));
		
		return info;
	}
	
	public static ThrowableInfo fromThrowable(ThrowableInformation ti) {
		if (ti == null)
			return null;
		if (ti.getThrowable() == null)
			return new ThrowableInfo(null, null, null, null);
		
		return fromThrowable(ti.getThrowable());
	}
	
	public static ThrowableInformation toThrowable(ThrowableInfo ti) {
		if (ti == null)
			return null;
		
		Throwable t;
		try {
			t = (Throwable) ThrowableInfo.class.getClassLoader().loadClass(ti.getClazz()).getConstructor(String.class).newInstance(ti.getMessage());
		}
		catch (Exception e) {
			t = new Exception(ti.getMessage());
		}
		
		ThrowableInformation cause = toThrowable(ti.getCause());
		t.initCause((cause == null) ? null : cause.getThrowable());
		t.setStackTrace(Arrays.stream(ti.getStackTrace()).map(LocationInfo::toStackTraceElement).toArray(size -> new StackTraceElement[size]));
		
		return new ThrowableInformation(t);
	}
}
