package com.here.acs.slogs.datatypes;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

public class SettableLoggingEvent {
    public static final PatternLayout LOG_EVENT_FORMAT = new PatternLayout("%d [%-5p] <%t> %C.%M(%F:%L) - %m");
    
    public static final Map<String, Category> CATEGORY_MAP = new LinkedHashMap<String, Category>() {
		private static final long serialVersionUID = -3899419027143866620L;
		
		private static final int MAX_ENTRIES = 200;

        protected boolean removeEldestEntry(Map.Entry<String, Category> eldest) {
           return size() > MAX_ENTRIES;
        }
    };

    private String fqnOfLoggerClass;
    private String level;
    private LocationInfo locationInfo;
    private String message;
    private String threadName;
    private long timeStamp;
    private String host;
    private ThrowableInfo exception;
    
    public SettableLoggingEvent() {}

    public SettableLoggingEvent(String fqnOfLoggerClass, String level, LocationInfo locationInfo,
    		String message, String threadName, long timeStamp, String host, ThrowableInfo exception) {
        this.fqnOfLoggerClass = fqnOfLoggerClass;
        this.level = level;
        this.locationInfo = locationInfo;
        this.message = message;
        this.threadName = threadName;
        this.timeStamp = timeStamp;
        this.host = host;
        this.exception = exception;
    }

    public void setFqnOfLoggerClass(String fqnOfLoggerClass) {
		this.fqnOfLoggerClass = fqnOfLoggerClass;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public void setLocationInfo(LocationInfo locationInfo) {
		this.locationInfo = locationInfo;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getFqnOfLoggerClass() {
        return fqnOfLoggerClass;
    }

    public String getLevel() {
        return level;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public String getMessage() {
        return message;
    }

    public String getThreadName() {
        return threadName;
    }

	public String getHost() {
		return host;
	}

    public long getTimeStamp() {
        return timeStamp;
    }

	public ThrowableInfo getException() {
		return exception;
	}

	public void setException(ThrowableInfo exception) {
		this.exception = exception;
	}

    public String toString() {
        return LOG_EVENT_FORMAT.format(toLoggingEvent(this));
    }

    public static SettableLoggingEvent fromLoggingEvent(LoggingEvent le) {
		if (le == null)
			return null;
		
		String host = "UnknownHost";
		try {
			host = Inet4Address.getLocalHost().getHostAddress();
		}
		catch (UnknownHostException leaveUnknown) {}
		
        return new SettableLoggingEvent(le.getFQNOfLoggerClass(), le.getLevel().toString(),
        		LocationInfo.fromLog4jLocationInfo(le.getLocationInformation()),
                le.getMessage().toString(), le.getThreadName(), le.getTimeStamp(), host,
                ThrowableInfo.fromThrowable(le.getThrowableInformation()));
    }

    public static LoggingEvent toLoggingEvent(SettableLoggingEvent sle) {
		if (sle == null)
			return null;
		
		String fqn = sle.getFqnOfLoggerClass();
		Category c = CATEGORY_MAP.get(fqn);
		if (c == null) {
			try {
				c = (Category) SettableLoggingEvent.class.getClassLoader().loadClass(fqn).getConstructor(String.class).newInstance((String) null);
			}
			catch (Exception e) {
				c = Logger.getLogger(SettableLoggingEvent.class);
			}
			CATEGORY_MAP.put(fqn, c);
		}

        return new LoggingEvent(fqn, c, sle.getTimeStamp(), Level.toLevel(sle.getLevel()), sle.getMessage(),
                sle.getThreadName(), ThrowableInfo.toThrowable(sle.getException()), null, 
                LocationInfo.toLog4jLocationInfo(sle.getLocationInfo()), null);
    }
}
