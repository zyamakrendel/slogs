package com.here.acs.slogs;

import java.net.InetSocketAddress;
import java.util.Arrays;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogServer {
	private static final Logger L = LoggerFactory.getLogger(LogServer.class);
	
    public static void main(String[] args) throws Exception {
    	L.info("Starting as {} {}", LogServer.class, Arrays.toString(args));
    	if (args.length != 1) {
    		L.error("Usage: {} <config file>", LogServer.class);
    		System.exit(1);
    	}
    	
    	L.info("Loading configuration...");
    	Config.init(args[0]);
    	Config config = Config.get();
    	L.info("Configuration loaded");
    	
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/");
        
        String host = config.getHost();
        InetSocketAddress addr = (host == null) ? 
        		new InetSocketAddress(config.getPort()) : new InetSocketAddress(host, config.getPort());
        
        L.info("Starting the server at {}...", addr);
        Server jettyServer = new Server(addr);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter("jersey.config.server.provider.classnames", EntryPoint.class.getCanonicalName());

        try {
            jettyServer.start();
            L.info("Server started, listening to requests");
            jettyServer.join();
        }
        finally {
            L.info("Stopping the server, bye!");
            jettyServer.destroy();
        }
    }
}
