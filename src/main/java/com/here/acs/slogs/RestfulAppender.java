package com.here.acs.slogs;

import javax.ws.rs.core.MediaType;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.here.acs.slogs.datatypes.SettableLoggingEvent;

import java.io.IOException;

public class RestfulAppender extends AppenderSkeleton {
    private static final ObjectWriter JSON_WRITER = (new ObjectMapper()).writerWithDefaultPrettyPrinter();

    private boolean initialized = false;
    
    private HttpClient client = null;
    private HttpHost target = null;
    private HttpPost request = null;

    private void initializeHttpIfPossible() {
        initializeHttpIfPossible(null);
    }

    private void initializeHttpIfPossible(String inetAddr) {
        if (initialized)
            return;

        if (inetAddr == null)
            inetAddr = System.getProperty("restful.logging.inetAddr");
    	if (inetAddr == null)
    		throw new IllegalStateException("no parameter to initialize RestfulAppender");

        String host;
        int port;
        try {
            String[] inetAddrComponents = inetAddr.split(":");
            if (inetAddrComponents.length != 2)
                throw new IllegalArgumentException("malformed INET address: " + inetAddr);
            host = inetAddrComponents[0];
            port = Integer.parseInt(inetAddrComponents[1]);
        }
        catch (Exception e) {
            throw new IllegalArgumentException("could not initialize internet end-point for log reporting");
        }

    	client = HttpClientBuilder.create().build();
    	target = new HttpHost(host, port);
    	request = new HttpPost("/log");
        request.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);

        initialized = true;
    }

    public void setTargetInetAddr(String inetAddr) {
        initializeHttpIfPossible(inetAddr);
    }
    
    public void append(LoggingEvent event) {
        initializeHttpIfPossible();

        try {
        	String json = JSON_WRITER.writeValueAsString(SettableLoggingEvent.fromLoggingEvent(event));
            StringEntity entity = new StringEntity(json);
            request.setEntity(entity);
        	HttpResponse response = client.execute(target, request);
        	StatusLine statusLine = response.getStatusLine();
        	if (statusLine.getStatusCode() >= 300) {
        		System.err.printf("could not report the log event: %s\n", SettableLoggingEvent.LOG_EVENT_FORMAT.format(event));
        		System.err.printf("the response received: %s\n", statusLine);
        	}
        }
        catch (Exception e) {
            System.err.println("could not send the RESTful request");
            e.printStackTrace(System.err);
        }
    }
    
    public boolean requiresLayout() {
        return false;
    }
    
    public void close() {}

    public static void main(String[] args) throws Exception {
        Logger L = Logger.getLogger(RestfulAppender.class);
        L.removeAllAppenders();
        
        RestfulAppender ra = new RestfulAppender();
        //-Drestful.logging.inetAddr=localhost:13080
        ra.setTargetInetAddr("localhost:13080");
        L.addAppender(ra);
        L.error("This message", new Exception("I'm an exception", new IOException("me too")));
    }
}
