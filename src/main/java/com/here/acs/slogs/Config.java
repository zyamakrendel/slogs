package com.here.acs.slogs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class Config {
	private static Config instance;
	
	public static void init(String configFile) throws IOException {
		init(configFile, false);
	}
	
	public static void init(String configFile, boolean ignoreRepeatedCalls) throws IOException {
		if (instance != null) {
			if (ignoreRepeatedCalls)
				return;
			throw new IllegalStateException("Config may not be initialized more than once");
		}
		
		instance = new Config(configFile);
	}
	
	public static Config get() {
		if (instance == null)
			throw new IllegalStateException("Config must be initialized before use");
		return instance;
	}
	
    public static InputStream loadFile(String filename) throws IOException {
        InputStream configFile = null;
        if (new File(filename).exists()) {
            try {
                configFile = new FileInputStream(filename);
            } catch (FileNotFoundException ignored) {
            }
        }

        if (configFile == null)
            configFile = Config.class.getClassLoader().getResourceAsStream(filename);

        if (configFile == null) {
            String msg = "File not found: " + filename;
            System.err.println(msg);
            throw new IOException(msg);
        }

        return configFile;
    }

    private static final String OPEN_VAR = "${";
    private static final String CLOSE_VAR = "}";
    
    public static String substituteVariables(String s, String ... subs) {
    	if (subs.length %2 != 0)
    		throw new IllegalArgumentException("odd number of substitutions - a list of pairs is expected");
    	
    	for (int i = 0; i < subs.length; i += 2)
    		s = s.replace(OPEN_VAR + subs[i] + CLOSE_VAR, subs[i + 1]);
    	
    	return s;
    }

    public static String readProperty(Properties props, String name) throws IllegalArgumentException {
        return readProperty(props, name, null);
    }

    public static String readProperty(Properties props, String name, String defaultValue) throws IllegalArgumentException {
        return props.getProperty(name, defaultValue);
    }

    public static int readPropertyInt(Properties props, String name) throws IllegalArgumentException {
        return readPropertyInt(props, name, null);
    }

    public static int readPropertyInt(Properties props, String name, String defaultValue) throws IllegalArgumentException {
        String value = readProperty(props, name, defaultValue);
        try {
            return Integer.parseInt(value.trim());
        }
        catch (NumberFormatException nfEx) {
            throw new IllegalArgumentException("configuration parameter missing or not an integer: " + name + " = " + value);
        }
    }
    
    public String readProperty(String name) throws IllegalArgumentException {
    	return readProperty(configProps, name);
    }
    
    public String readProperty(String name, String defaultValue) throws IllegalArgumentException {
    	return readProperty(configProps, name, defaultValue);
    }
    
    public int readPropertyInt(String name) throws IllegalArgumentException {
    	return readPropertyInt(configProps, name);
    }
    
    public int readPropertyInt(String name, String defaultValue) throws IllegalArgumentException {
    	return readPropertyInt(configProps, name, defaultValue);
    }

	private int port;
	private String host;
	private String logFileTemplate;
	private Properties configProps;
	private Properties log4jProps;
	
	private Config(String configFile) throws IOException {
		configProps = new Properties();
		configProps.load(loadFile(configFile));
		
		port = readPropertyInt("server.port", "80");
		host = readProperty("server.host");
		logFileTemplate = readProperty("log.file.template");
		
		log4jProps = new Properties();
		configProps.entrySet().stream()
			.filter(pair -> pair.getKey().toString().startsWith("log4j."))
			.forEach(pair -> log4jProps.setProperty(pair.getKey().toString(), pair.getValue().toString()));
	}

	public int getPort() {
		return port;
	}

	public String getHost() {
		return host;
	}
	
	public Properties getLog4jProps() {
		return log4jProps;
	}

	public String getLogFileTemplate() {
		return logFileTemplate;
	}
	
	public Properties getProperties() {
		return configProps;
	}
}
