package com.here.acs.slogs;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Appender;
import org.apache.log4j.Hierarchy;
import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.spi.RootLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.here.acs.slogs.datatypes.SettableLoggingEvent;


@Path("/")
public class EntryPoint {
	private static final Logger L = LoggerFactory.getLogger(EntryPoint.class);
	
	private static final String DEFAULT_FILE_NAME = "EMR_LOGGER.log";
	private static final String JAVA_FILE_ENDING = ".java";
	private static final String CONFIG_FILE = "config.properties";
	private static final String APPENDER_NAME_PREFIX = "APP_";
	private static final String APPENDER_NAME_PARAM = "APPENDER_NAME";
	
	private static final AtomicLong APPENDER_COUNTER = new AtomicLong();
	
	private static final Config CONFIG;
	
	static {
		try {
			Config.init(CONFIG_FILE, true);
		}
		catch (IOException ioEx) {
			String msg = "Fatal error - could not load config file" + CONFIG_FILE;
			L.error(msg, ioEx);
			throw new RuntimeException(msg, ioEx);
		}
		
    	CONFIG = Config.get();
	}
	
	private final Map<String, Appender> logFileWriterMap = new HashMap<>();
	
    @GET
    @Path("ping")
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
    	L.debug("Pinged - ponged");
        return "Pong!";
    }
    
    private String createLogFileName(SettableLoggingEvent sle) {
    	if (sle == null)
    		return DEFAULT_FILE_NAME;
    	
    	String host = sle.getHost();
    	String eventFileName = sle.getLocationInfo().getFileName();
    	if (eventFileName.endsWith(JAVA_FILE_ENDING))
    		eventFileName = eventFileName.substring(0, eventFileName.length() - JAVA_FILE_ENDING.length());
    	
    	String logFileName = CONFIG.getLogFileTemplate();
    	logFileName = Config.substituteVariables(logFileName, "host", host);
    	logFileName = Config.substituteVariables(logFileName, "fileName", eventFileName);
    	
    	return logFileName;
    }
    
    private Appender createAppender(String logFileName) {
    	String appenderName = APPENDER_NAME_PREFIX + APPENDER_COUNTER.incrementAndGet();
    	Properties props = new Properties();
    	Config.get().getLog4jProps().entrySet().stream()
    		.forEach(pair -> props.setProperty(
    			Config.substituteVariables(pair.getKey().toString(), APPENDER_NAME_PARAM, appenderName),
    			Config.substituteVariables(pair.getValue().toString(), APPENDER_NAME_PARAM, appenderName, "logFileName", logFileName)));
    	
    	Hierarchy loggerRepository = new Hierarchy(new RootLogger((Level) Level.DEBUG));
    	(new PropertyConfigurator()).doConfigure(props, loggerRepository);
    	
    	return loggerRepository.getRootLogger().getAppender(appenderName);
    }
    
    private Appender obtainLogAppender(String logFileName) throws IOException {
    	Appender a = logFileWriterMap.get(logFileName);
    	if (a == null) {
    		a = createAppender(logFileName);
    		logFileWriterMap.put(logFileName, a);
    	}
    	
    	return a;
    }

    @POST
    @Path("log")
    @Consumes(MediaType.APPLICATION_JSON)
    public void log(SettableLoggingEvent sle) {
    	L.debug("A new logging event: {}", sle);
    	
    	try {
    		String logFileName = createLogFileName(sle);
    		Appender a = obtainLogAppender(logFileName);
    		a.doAppend(SettableLoggingEvent.toLoggingEvent(sle));
    		
    		L.debug("logged successfully");
    	}
    	catch (Exception e) {
    		L.error("error encountered while trying to log the event: " + sle, e);
    	}
    }
}
